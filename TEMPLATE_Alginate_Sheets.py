##### Import Statements ######
from collections import defaultdict
from typing import DefaultDict
from opentrons import protocol_api
import csv
from opentrons.commands.commands import air_gap, drop_tip
from opentrons.types import Point
import math
import subprocess
import numpy as np

csv_name = "alginate_sheets_CSV.csv"

NUM_PLATES = 1  # currently 1-6
ALGINATE_MIX_REPS = 3  # HOW MANY TIMES ALGINATE WILL BE MIXED
DEFAULT_VOLUME_FOR_ALGINATE_TRANSFER_UL = 5  # Only used for reagent calculations
ALGINATE_MIX_VOLUME = 10  # volume p20 pipette mixes at
CACL2_VOLUME_UL = 100  # 100 UL volume cacl2 default is used in the wash step
MEDIA_VALUE_UL = 75  # 15 * lasgana volume is typically used
MEDIA_WASH_AMOUNTS = 1  # How many DMEM wash steps you want (default 2)

## ------------Labware assignments---------------##
# Change these based upon the labware you have, remember to upload your labware to the opentrons itself#
PLATES_YOU_ARE_USING = "falcon_353219_black_96_wellplate_392ul"

TUBE_RACK_USED = "microamp_96well_pcr_aluminum_block"

## ------------Delays and Incubations---------------##
step_2_delay_minutes = 0
delay_after_dispension_of_cacl2 = 5
p20_starting_tip_location = "A1"
p300_starting_tip_location = "A1"

###### DO NOT EDIT PLEASE #####

HEIGHT_ABOVE_BOTTOM = 0.6  # mm above well bottom for alginate alginate_sheets

mix_alginate = True  # This is for testing, if physically testing = False to save time

# -------------------FLOW RATES-----------------------
p20_DEFAULT_FLOW_RATE = 7.56
p300_DEFAULT_FLOW_RATE = 100
Alginate_aspirate_flow_rate = p20_DEFAULT_FLOW_RATE / 1.5
ALGINATE_FLOW_RATE = p20_DEFAULT_FLOW_RATE / 5
ALGINATE_MIX_FLOW_RATE = p300_DEFAULT_FLOW_RATE
CACL2_FLOW_RATE = p300_DEFAULT_FLOW_RATE / 8
DMEM_FLOW_RATE = p300_DEFAULT_FLOW_RATE / 5

# REAGENT LOCATIONS:
reagent_locs = {
    # columns in base_tuberack
    "CaCl": ["A1"],
    "DMEM": ["A2"],
}
# convert to 2 well reservoir when we have specs
dead_volume = {
    "opentrons_6_tuberack_falcon_50ml_conical": 2000,
    "nest_1_reservoir_195ml": 19000,
    "nest_12_reservoir_15ml": 1500,
    "agilent_2_well_reservoir_146ml": 4500,
    "microamp_96well_pcr_aluminum_block": 5,
}

max_volume = {
    "opentrons_6_tuberack_falcon_50ml_conical": 49000,
    "nest_1_reservoir_195ml": 190000,
    "nest_12_reservoir_15ml": 14500,
    "agilent_2_well_reservoir_146ml": 140000,
    "microamp_96well_pcr_aluminum_block": 200,
}

# ======  DO NOT EDIT UNLESS YOU VAGUELY KNOW WHAT YOU'RE DOING  ======= #


# Metadata
metadata = {
    "protocolName": "alginate_sheets/Alginate Disc Protocol for Fat Culture",
    "description": "Seed wells with alginate/cell mixture in a disc morphology",
    "Joe Caponi": "joe@mosameat.com",
    "apiLevel": "2.10",
}


def run(protocol: protocol_api.ProtocolContext):

    ################## LOAD LABWARE#################
    plate_locs = [1, 2, 3, 4, 5, 6]  # location of plates in slots
    # Well Plates
    all_plates_dict = {}
    for plate_ind in range(NUM_PLATES):
        plate_num = plate_ind + 1
        all_plates_dict[plate_ind] = protocol.load_labware(
            PLATES_YOU_ARE_USING,
            plate_locs[plate_ind],
            label="wellplate #{}".format(plate_num),
        )

    all_plates = list(all_plates_dict.values())

    # ----------Load Reagents (Tubes or reservoirs)--------------#

    reservoir_for_waste = protocol.load_labware("nest_1_reservoir_195ml", 10)

    input_tube_rack = protocol.load_labware(
        TUBE_RACK_USED, 7, label="Alginate Tube Location"
    )

    Reagent_Trough = protocol.load_labware(
        "agilent_2_well_reservoir_146ml", 8, label="Reagent Trough"
    )

    # Tip Racks
    tips_multi_200 = protocol.load_labware("opentrons_96_filtertiprack_200ul", 9)
    tips_20 = protocol.load_labware("opentrons_96_filtertiprack_20ul", 11)

    # LOAD PIPETTE(S)

    p20_multi = protocol.load_instrument("p20_multi_gen2", "right", tip_racks=[tips_20])
    p300_multi = protocol.load_instrument(
        "p300_multi_gen2", "left", tip_racks=[tips_multi_200]
    )
    p20_multi.starting_tip = tips_20.well(p20_starting_tip_location)
    p300_multi.starting_tip = tips_multi_200.well(p300_starting_tip_location)

    ASPIRATE_OFFSET = ((all_plates[0].well("A1").diameter) / 2) - 1.8

    # will aspirate at bottom of well slightly above alginate_sheets >>>>><<<<<<<
    # --------------##### Functions #####----------------------
    def change_flow_rate_p300(changed_flowrate):
        protocol.comment(
            "Changing multichannel p300 flow rate to {}".format(changed_flowrate)
        )
        p300_multi.flow_rate.aspirate = changed_flowrate
        p300_multi.flow_rate.dispense = changed_flowrate

    def change_flow_rate_p20(changed_flowrate):
        protocol.comment(
            "Changing multichannel p20 flow rate to {}".format(changed_flowrate)
        )
        p20_multi.flow_rate.aspirate = changed_flowrate
        p20_multi.flow_rate.dispense = changed_flowrate

    
    ################################CSV STUFF####################################

    def transfer_csv_to_list_ordered_by_cols(csv_name):

        # open csv, read-only
        try:
            folder_path = ""
            file_path_sample_destination_locations = folder_path + csv_name

            filename_csv_sample_destination_locations = open(
                file_path_sample_destination_locations, "r"
            )
        except:
            folder_path = "/var/lib/jupyter/notebooks/"
            file_path_sample_destination_locations = folder_path + csv_name

            filename_csv_sample_destination_locations = open(
                file_path_sample_destination_locations, "r"
            )

        # creating empty lists for cols
        col_1 = []
        col_2 = []
        col_3 = []
        col_4 = []
        col_5 = []
        col_6 = []
        col_7 = []
        col_8 = []
        col_9 = []
        col_10 = []
        col_11 = []
        col_12 = []

        all_cells_ordered_by_cols = []

        # creating dictreader object
        file = csv.DictReader(filename_csv_sample_destination_locations)

        for col in file:
            col_1.append(col["1"])
            col_2.append(col["2"])
            col_3.append(col["3"])
            col_4.append(col["4"])
            col_5.append(col["5"])
            col_6.append(col["6"])
            col_7.append(col["7"])
            col_8.append(col["8"])
            col_9.append(col["9"])
            col_10.append(col["10"])
            col_11.append(col["11"])
            col_12.append(col["12"])

        all_cells_ordered_by_cols = (
            col_1
            + col_2
            + col_3
            + col_4
            + col_5
            + col_6
            + col_7
            + col_8
            + col_9
            + col_10
            + col_11
            + col_12
        )

        filename_csv_sample_destination_locations.close()

        return all_cells_ordered_by_cols

    # create list of all wells of 96 well plate:

    def create_list_of_96_well_plate_ordered_by_col():
        all_well_names_ordered_by_cols = []
        rows_as_string = ["A", "B", "C", "D", "E", "F", "G", "H"]
        cols_as_string = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        for col in cols_as_string:
            for row in rows_as_string:
                all_well_names_ordered_by_cols.append(row + col)

        return all_well_names_ordered_by_cols

    all_well_names_ordered_by_cols = create_list_of_96_well_plate_ordered_by_col()

    csv_volumes_as_list = transfer_csv_to_list_ordered_by_cols(csv_name)

    csv_volumes_as_dict = {}
    for well, vol_sample_name in zip(
        all_well_names_ordered_by_cols, csv_volumes_as_list
    ):
        split_vol_sample = vol_sample_name.split(" - ")
        if len(split_vol_sample) > 1:

            csv_volumes_as_dict[well] = {
                "volume": float(split_vol_sample[0]),
                "sample_name": split_vol_sample[1],
            }
    # find all unique compound names
    unique_names = []
    for vol_and_name in csv_volumes_as_dict.values():
        name_only = vol_and_name["sample_name"]
        if name_only not in unique_names:
            if name_only != "":
                unique_names.append(name_only)

    # assign source locations to compound names
    compound_placement_dict = {}
    for well, name in zip(input_tube_rack.rows()[0], unique_names):
        compound_placement_dict[name] = well

    total_volume_dict_per_isolation = {}
    for name in unique_names:
        sample_track = 0
        for value in csv_volumes_as_dict.values():
            # Objective = modify reagent_locs to have a dictionary value for each isolation
            if value["sample_name"] == name:
                sample_track += value["volume"]
            else:
                pass

        sample_track = sample_track * NUM_PLATES
        sample_track += dead_volume[TUBE_RACK_USED]
        # Returns dict with how much volume per isolation you need
        # Will be used later in reagent tracker and print total reagent
        total_volume_dict_per_isolation[name] = sample_track

    # print("total_volume_dict_per_isolation", total_volume_dict_per_isolation)

    list_of_tuberack_cols_by_name = list(input_tube_rack.columns_by_name().keys())[::-1]
    isolation_locations = []

    for item in list_of_tuberack_cols_by_name:
        isolation_locations.append("A" + item)

    # ^^Backwards list of wells in first row of input tubes
    for name, volume in total_volume_dict_per_isolation.items():
        reagent_locs[name] = []
        popped_iso = isolation_locations.pop()
        reagent_locs[name].append(popped_iso)
        # basically adds wells to reagent_locs
        while (
            volume
            > max_volume[input_tube_rack.load_name]
            - dead_volume[input_tube_rack.load_name]
        ):
            popped_iso = isolation_locations.pop()
            reagent_locs[name].append(popped_iso)
            ### Checks to see if there's overflow, then assigns the next well in line
            # This ensures everybody has enough wells in reagent_locs
            volume -= (
                max_volume[input_tube_rack.load_name]
                - dead_volume[input_tube_rack.load_name]
            )
    protocol.comment(
        ">> LABWARE INFO, ADD THE FOLLOWING uL OF {} into the specified wells".format(
            name
        )
    )
    for name in unique_names:
        count = 0
        total_vol = total_volume_dict_per_isolation[name]

        while total_vol > max_volume[TUBE_RACK_USED]:
            protocol.comment(
                "{} of {} in {}".format(
                    max_volume[TUBE_RACK_USED],
                    name,
                    input_tube_rack.wells(reagent_locs[name][count]),
                )
            )
            count += 1
            total_vol -= max_volume[TUBE_RACK_USED]
        protocol.comment(
            "{} of {} in {}".format(
                total_vol + dead_volume[TUBE_RACK_USED],
                name,
                input_tube_rack.wells(reagent_locs[name][count]),
            )
        )

        # subtracts that amount from total volume needed to see if we have overflow
    ############# REAGENT TRACKER ########################
    # INITIALIZE REAGENT LOCATIONS
    reagent_tracker = {
        "CaCl": {
            "ind": 0,
            "remaining_vol": max_volume[Reagent_Trough.load_name],
            "reset_vol": max_volume[Reagent_Trough.load_name],
            "dead_vol": dead_volume[Reagent_Trough.load_name],
        },
        "DMEM": {
            "ind": 0,
            "remaining_vol": max_volume[Reagent_Trough.load_name],
            "reset_vol": max_volume[Reagent_Trough.load_name],
            "dead_vol": dead_volume[Reagent_Trough.load_name],
        },
        "waste": {
            "ind": 0,
            "remaining_vol": max_volume[reservoir_for_waste.load_name],
            "reset_vol": max_volume[reservoir_for_waste.load_name],
            "dead_vol": 0,
        },
        "Cell": {
            "ind": 0,
            "remaining_vol": max_volume[input_tube_rack.load_name],
            "reset_vol": max_volume[input_tube_rack.load_name],
            "dead_vol": 0,
        },
    }
    for name in unique_names:
        reagent_tracker[name] = {
            "ind": 0,
            "remaining_vol": max_volume[input_tube_rack.load_name],
            "reset_vol": max_volume[input_tube_rack.load_name],
            "dead_vol": dead_volume[input_tube_rack.load_name],
        }
    print(reagent_tracker)

    def track_reagent(reagent_name_string, step_volume, pip):
        # makes sure there is enough reagent in the current column; if not, goes to next column

        # (1) remove the volume from the current column of reagent
        if pip == p20_multi:
            reagent_tracker[reagent_name_string]["remaining_vol"] -= step_volume

        # ^^we have an if statement here because our p20 multi is taking stuff away from individual wells in a pcr plate
        else:
            reagent_tracker[reagent_name_string]["remaining_vol"] -= (
                step_volume * pip.channels
            )

        protocol.comment(
            "REAGENT TRACKER -- {}, removed {}, remaining {}".format(
                reagent_name_string,
                step_volume,
                reagent_tracker[reagent_name_string]["remaining_vol"],
            )
        )
        # print(reagent_tracker[reagent_name_string]['remaining_vol'])

        # if below dead_volume, change columns for the next time you want to work with this reagent
        if (
            reagent_tracker[reagent_name_string]["remaining_vol"]
            < reagent_tracker[reagent_name_string]["dead_vol"] + 2.5
        ):
            # (2) moves to next column of that reagent
            reagent_tracker[reagent_name_string]["ind"] += 1
            protocol.comment(
                "remaining vol = {}; switching to col {}".format(
                    reagent_tracker[reagent_name_string]["remaining_vol"],
                    reagent_tracker[reagent_name_string]["ind"],
                )
            )
            # print('{} remaining vol = {}; switching to col {}'.format(reagent_name_string,
            #       reagent_tracker[reagent_name_string]['remaining_vol'], reagent_tracker[reagent_name_string]['ind']))

            # (3) resets reminaing volume of the new column to maximum volume of column
            reagent_tracker[reagent_name_string]["remaining_vol"] = reagent_tracker[
                reagent_name_string
            ]["reset_vol"]

    def well_or_loc(entry, pipette):
        # checks whether entry is well or well.top()/well.bottom()
        well_with_depth = entry
        try:  # just a labware without location
            entry.top()
            well_just_well = entry
        except:  # when source is already a Location() (has .top() or .bottom() included)
            well_just_well = entry.labware
        return [well_just_well, well_with_depth]

    # @@@@@@@@@@@---------@@@@@@@@@@TRACKED TRANSFER@@@@@@@@@@@---------@@@@@@@@@@#

    # ----------------------- PRINT REAGENT TOTAL VOLUME ----------------------#

    def print_reagent_total_volume(
        step_volume,
        reagent_name_string,
        labware,
        locations_list,
        number_plates=NUM_PLATES,
        wells_per_plate=96,
        multiple_rounds=1,
    ):

        # calculate total volume

        total_vol = (
            wells_per_plate
            * step_volume
            * multiple_rounds
            * number_plates
            * (len(csv_volumes_as_dict))
            / 12
        )

        protocol.comment(
            ">>> LABWARE INFO >> Add the following volumes of {} to the following wells:".format(
                reagent_name_string
            )
        )

        # useable volume in a trough well
        max_trough_working_volume = (
            max_volume[labware.load_name] - dead_volume[labware.load_name]
        )
        # number of trough wells you need of given reagent
        used_wells = math.ceil(total_vol / max_trough_working_volume)
        # print(reagent_name_string, used_wells)

        # check that you have enough of reagent
        all_cols = locations_list
        max_total_volume = len(all_cols) * max_trough_working_volume
        if total_vol > max_total_volume:
            raise ValueError(
                "Too much {} required. Need more columns in reagents trough; required = {} uL, max current volume = {} uL".format(
                    reagent_name_string, total_vol, max_total_volume
                )
            )

        leftover_vol = total_vol
        for well in locations_list[:used_wells]:
            # print(well)
            if (
                leftover_vol > max_trough_working_volume
            ):  # if less that trough well working volume, fill trough well
                well_vol = max_trough_working_volume + dead_volume[labware.load_name]
                leftover_vol -= max_trough_working_volume
            else:  # if you are on the last well that does not need to be filled
                well_vol = leftover_vol + dead_volume[labware.load_name]
                leftover_vol -= leftover_vol
            protocol.comment(
                "    {} uL {} in {} on {}".format(
                    well_vol, reagent_name_string, well, labware
                )
            )

        if leftover_vol != 0:
            raise ValueError(
                "leftover_vol: {} uL, something went wrong in print_reagent_total_volume()".format(
                    leftover_vol
                )
            )

    print_reagent_total_volume(
        step_volume=CACL2_VOLUME_UL,
        reagent_name_string="CaCl",
        labware=Reagent_Trough,
        locations_list=reagent_locs["CaCl"],
    )

    print_reagent_total_volume(
        step_volume=MEDIA_VALUE_UL,
        reagent_name_string="DMEM",
        labware=Reagent_Trough,
        locations_list=reagent_locs["DMEM"],
        multiple_rounds=MEDIA_WASH_AMOUNTS,
    )

    def p20_multi_mix(alginate_mix_vol):

        if mix_alginate == True:
            protocol.comment("Mixing Cell Solution")
            change_flow_rate_p20(p20_DEFAULT_FLOW_RATE * 2)
            p20_multi.mix(ALGINATE_MIX_REPS, alginate_mix_vol, source_name.bottom(1.0))
            p20_multi.move_to(source_name.bottom(1))
            p20_multi.move_to(source_name.top(0.7))
            p20_multi.move_to(source_name.bottom(1))
            change_flow_rate_p20(p20_DEFAULT_FLOW_RATE)

    # -----------------------------------------------------------------------------
    ##################################################################
    # ---------- BEGIN LIQUID HANDLER STEPS -----------------

    # (0) PROTOCOL COMMENTS
    # Information about how much volume of each reagent is needed, where to put plates and tubes, etc.
    protocol.comment(">>> CHECK THESE NOTES BEFORE STARTING THE RUN:")
    protocol.comment(
        "Please ensure that each Alginate tube is placed in sequential order of how they're put in the CSV and placed into the eppendorf rack)"
    )
    protocol.comment("example: eppendorf rack A1,A2,A3,A4")

    protocol.comment(
        "Please remember to remove all lids from labware before starting, lest you be responsible for the end of the world as we know it"
    )

    protocol.comment(
        "If you notice any issues with this protocol or changes you would like implemented, please contact Joe or Jane on Slack"
    )
    protocol.comment("Thank you for choosing Mosa Meat Laboratory Automation")

    # (1) Transfer alginate to well plates from falcon tube -----------------

    protocol.comment("Picking up tips for mixing and alginate distribution")

    #####Necessary Variables ######
    mix_wellcount = 0
    blow_out_wellcount = 0

    previous_sample = "first sample"
    previous_sample_location = "somewhere"
    ##########################

    # Aspirate and dispense for each group
    p20_multi.pick_up_tip()
    # mix step before starting
    protocol.comment("STATUS >> (1a) Dispensing alginate cell mixture to plate")

    tip_count = 1
    # This will save tips by just pipetting all of the samples of one kind, and then another
    for name in unique_names:
        for well_name in csv_volumes_as_dict.keys():
            if csv_volumes_as_dict[well_name]["sample_name"] == name:
                for plate in all_plates:
                    step_volume = csv_volumes_as_dict[well_name]["volume"]
                    sample_name = csv_volumes_as_dict[well_name]["sample_name"]
                    source_name = input_tube_rack.well(
                        reagent_locs[sample_name][reagent_tracker[sample_name]["ind"]]
                    )
                    dest_loc = plate.well(well_name)

                    # want to check if new tube is going to be used, so we can change tip
                    if previous_sample == "first sample":
                        protocol.comment("Initial Mix step for the poor sad cells")
                        p20_multi_mix(ALGINATE_MIX_VOLUME)
                    if (
                        previous_sample != sample_name
                        and previous_sample != "first sample"
                    ):
                        protocol.comment("Changing Tip for new sample")
                        tip_count += 1
                        protocol.comment(
                            "'Arrr, any sufficiently advanced technology be indistinguishable from demonic posession'"
                        )
                        p20_multi.blow_out(previous_sample_location.top(-3))
                        p20_multi.move_to(previous_sample_location.bottom(0.7))
                        p20_multi.drop_tip()
                        p20_multi.pick_up_tip()
                        blow_out_wellcount = 0
                        protocol.comment("Mixing Cell Solution")
                        p20_multi_mix(ALGINATE_MIX_VOLUME)
                        mix_wellcount = 0
                        blow_out_wellcount = 0
                    protocol.comment(
                        "Transferring {} uL of {} into {}".format(
                            step_volume, sample_name, dest_loc
                        )
                    )
                    p20_multi.flow_rate.aspirate = Alginate_aspirate_flow_rate
                    p20_multi.aspirate(step_volume, source_name.bottom(0.7))
                    protocol.delay(seconds=2)

                    p20_multi.move_to(source_name.top())

                    p20_multi.flow_rate.dispense = ALGINATE_FLOW_RATE
                    p20_multi.dispense(
                        step_volume, dest_loc.bottom(HEIGHT_ABOVE_BOTTOM)
                    )
                    protocol.delay(seconds=2)
                    blow_out_wellcount += 1
                    mix_wellcount += 1
                    # Here we change the name of previous sample to the current sample, so we can compare it after the loop
                    previous_sample = sample_name
                    # we also store the location of the previous sample location so we can blow out there and not waste cells(does this matter? idk)
                    previous_sample_location = source_name

                    if blow_out_wellcount == 4:
                        p20_multi.blow_out(source_name.top(-2))
                        blow_out_wellcount = 0
                    if mix_wellcount == 8:
                        p20_multi.blow_out(source_name.top(-2))
                        p20_multi.move_to(source_name.bottom(0.7))

                        p20_multi_mix(ALGINATE_MIX_VOLUME)
                        mix_wellcount = 0

    p20_multi.drop_tip()
    print("You will need {} p20 tip columns in total".format(tip_count))
    # (2) Incubation
    protocol.comment(
        ">> STATUS (2) >> Allowing alginate to rest for {} minutes".format(
            step_2_delay_minutes
        )
    )
    protocol.delay(minutes=step_2_delay_minutes)
    ################ WASHING STEPS START HERE ##################################
    # (3) Wash with CaCl2 slowly / carefully at sides.  --------------------------
    # WANT TO DISPENSE AT SIDE OF WELL, NOT SURE IF I DID THIS CORRECTLY, PROBABLY NOT = (
    protocol.comment(
        ">> STATUS >> (3) Washing wells with {}uL CaCl2, taking care to not disturb the baking sheets".format(
            CACL2_VOLUME_UL
        )
    )

    p300_multi.pick_up_tip()

    # make a list of wells in the top row of the 96 well plate

    for well_name in csv_volumes_as_dict.keys():
        for plate in all_plates:
            dest_loc = plate.well(well_name)

            change_flow_rate_p300(p300_DEFAULT_FLOW_RATE)
            protocol.comment("Adding CaCl2")
            p300_multi.aspirate(
                CACL2_VOLUME_UL,
                Reagent_Trough.well(
                    reagent_locs["CaCl"][reagent_tracker["CaCl"]["ind"]]
                ),
            )

            change_flow_rate_p300(CACL2_FLOW_RATE)

            p300_multi.dispense(
                CACL2_VOLUME_UL,
                dest_loc.bottom(1).move(Point(x=ASPIRATE_OFFSET, y=0, z=0)),
            )

            protocol.delay(seconds=5)
            track_reagent(
                reagent_name_string="CaCl", step_volume=CACL2_VOLUME_UL, pip=p300_multi
            )
        # for plate in all_plates:
        for plate in all_plates:
            dest_loc = plate.well(well_name)
            change_flow_rate_p300(CACL2_FLOW_RATE)
            protocol.comment("Aspirating CaCl2")
            p300_multi.aspirate(
                CACL2_VOLUME_UL,
                dest_loc.bottom(1).move(Point(x=ASPIRATE_OFFSET, y=0, z=0)),
            )

            change_flow_rate_p300(p300_DEFAULT_FLOW_RATE)

            p300_multi.dispense(CACL2_VOLUME_UL, reservoir_for_waste["A1"].top())
            p300_multi.blow_out(reservoir_for_waste["A1"].top())
            p300_multi.move_to(reservoir_for_waste["A1"].bottom(0.5))
    p300_multi.drop_tip()
    # (4) WASH 2X WITH DMEM -----------------------------

    protocol.comment(
        "STATUS >> (4a) Washing wells with {}uL DMEM for first time".format(
            MEDIA_VALUE_UL
        )
    )
    protocol.comment(
        "Changing multichannel p300 flow rate to {}".format(DMEM_FLOW_RATE)
    )
    # washes with DMEM, based upon how many DMEM washes were specified
    p300_multi.pick_up_tip()
    current_wash_number = 0
    while MEDIA_WASH_AMOUNTS > current_wash_number:

        for plate in all_plates:
            for well_name in csv_volumes_as_dict.keys():
                dest_loc = plate.well(well_name)
                change_flow_rate_p300(p300_DEFAULT_FLOW_RATE)
                protocol.comment("Transferring DMEM to wells")
                p300_multi.aspirate(
                    MEDIA_VALUE_UL,
                    Reagent_Trough.well(
                        reagent_locs["DMEM"][reagent_tracker["DMEM"]["ind"]]
                    ),
                )

                change_flow_rate_p300(DMEM_FLOW_RATE)
                p300_multi.dispense(
                    MEDIA_VALUE_UL,
                    dest_loc.bottom(1).move(Point(x=ASPIRATE_OFFSET, y=0, z=0)),
                )

                track_reagent(
                    reagent_name_string="DMEM",
                    step_volume=MEDIA_VALUE_UL,
                    pip=p300_multi,
                )

                protocol.delay(seconds=1)
        if current_wash_number < MEDIA_WASH_AMOUNTS - 1:

            for plate in all_plates:
                for well_name in csv_volumes_as_dict.keys():
                    dest_loc = plate.well(well_name)
                    protocol.comment("Aspirating DMEM")
                    p300_multi.aspirate(
                        MEDIA_VALUE_UL,
                        dest_loc.bottom(1).move(Point(x=ASPIRATE_OFFSET, y=0, z=0)),
                    )
                    # change flow rate to speed up run, doesn't matter how fast rate
                    # is above the waste!
                    change_flow_rate_p300(p300_DEFAULT_FLOW_RATE)

                    p300_multi.dispense(MEDIA_VALUE_UL, reservoir_for_waste["A1"].top())
                    p300_multi.blow_out(reservoir_for_waste["A1"].top())
                    # Dip into waste
                    p300_multi.move_to(reservoir_for_waste["A1"].bottom(0.5))

                    track_reagent(
                        reagent_name_string="waste",
                        step_volume=MEDIA_VALUE_UL,
                        pip=p300_multi,
                    )
            p300_multi.drop_tip()
            p300_multi.pick_up_tip()
        # Increase wash number by 1, if it's value is 1, then it doesn't aspirate off remaining media and just keeps it there
        current_wash_number += 1

    p300_multi.drop_tip()
    protocol.comment("Hurray! You're done!")
