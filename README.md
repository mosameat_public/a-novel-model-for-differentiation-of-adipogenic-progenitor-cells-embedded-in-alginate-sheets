## A Novel Model for Differentiation of Adipogenic Progenitor Cells Embedded in Alginate Sheets

====================
## Description

These are the necessary files needed to run the opentrons protocol titled "A Novel Model for Differentiation of Adipogenic Progenitor Cells Embedded in Alginate Sheets". To run this protocol, the following is needed.  Note: These are simply what was used in our specific example. Equivilent equipment will work, so long as the proper labware definitions are [made](https://labware.opentrons.com/create/) / [used](https://labware.opentrons.com/)

- Opentrons OT2-R or OT-2 unit equipped with:
- Opentrons p300 8-Channel Electronic Pipette gen2
- Opentrons p20 8-Channel Electronic Pipette gen2

- The plexiglass that the OT-2R unit comes pre-installed with should be removed after placing the unit into a laminar flow hood, both for airflow purposes and for ease-of-access by scientists setting up the liquid handler for usage
- Spetec 56 laminar flow box (or similar) 
- A standard Windows-based laptop with an i5 4th Generation Intel Core Processor connected to the OT-2 unit via USB or WiFi (almost any modern laptop would be sufficient)
- Opentrons [20 µL](https://shop.opentrons.com/opentrons-20ul-filter-tips/) and [200 µL filter tips ](https://shop.opentrons.com/opentrons-200ul-filter-tips/)
- MicroAmp™ Optical 96-Well Reaction Plate Applied Biosystems™ (CAT#N8010560) placed on top of a [96-well aluminum block from Opentrons](https://shop.opentrons.com/aluminum-block-set/)
- Alternatively: Standard PCR reaction tubes 
- Alternatively: 96-Well PCR Plates from Biorad (HSP9601) (no aluminum block required) 
- Agilent 2 well SBS polypropylene reservoir (203852-100).
- NEST 96 pyramid bottom SBS reservoir (pn 360103)
- 96 well imaging plates e.g. Falcon® 96-well Black/Clear Flat Bottom TC-treated Imaging Microplate with Lid (Falcon, 353219)
- Alternatively: Any similar microplate can be used


## Usage

To use these files on an opentrons unit, please follow the guide listed on the [opentrons website](https://support.opentrons.com/s/article/Get-started-Run-your-protocol)

## Authors and acknowledgment
Joseph Caponi and Jane Shmushkis contributed to making this project possible

## License
Apache v2.0


